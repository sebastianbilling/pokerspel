import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class PokerTest {

  @Test
  void testForFlush() {
    HandVariant handTest = new HandVariant();
    ArrayList<Card> tempHand = new ArrayList<>();

    tempHand.add(new Card(Rank.TWO, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.FOUR, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.ACE, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.JACK, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.NINE, Suit.DIAMONDS));

    assertTrue(handTest.isFlush(tempHand));
  }

  @Test
  void testForPair() {
    HandVariant handTest = new HandVariant();
    ArrayList<Card> tempHand = new ArrayList<>();

    tempHand.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.FIVE, Suit.CLUBS));
    tempHand.add(new Card(Rank.FIVE, Suit.HEARTS));
    tempHand.add(new Card(Rank.FOUR, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.ACE, Suit.SPADES));

    assertTrue(handTest.pairCard(tempHand));
  }



  @Test
  void testForThreeOfAKind() {
    HandVariant handTest = new HandVariant();
    ArrayList<Card> tempHand = new ArrayList<>();

    tempHand.add(new Card(Rank.JACK, Suit.HEARTS));
    tempHand.add(new Card(Rank.TEN, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.TEN, Suit.CLUBS));
    tempHand.add(new Card(Rank.TEN, Suit.SPADES));
    tempHand.add(new Card(Rank.FOUR, Suit.HEARTS));

    assertTrue(handTest.isThreeOfAKind(tempHand));

  }

  @Test
  void testForFourOfAKind() {
    HandVariant handTest = new HandVariant();
    ArrayList<Card> tempHand = new ArrayList<>();

    tempHand.add(new Card(Rank.JACK, Suit.HEARTS));
    tempHand.add(new Card(Rank.FOUR, Suit.CLUBS));
    tempHand.add(new Card(Rank.FOUR, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.FOUR, Suit.SPADES));
    tempHand.add(new Card(Rank.FOUR, Suit.HEARTS));

    assertTrue(handTest.isFourOfAKind(tempHand));

  }


  @Test
  void testForTwoPair() {
    HandVariant handTest = new HandVariant();
    ArrayList<Card> tempHand = new ArrayList<>();
    tempHand.add(new Card(Rank.TEN, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.TEN, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.FIVE, Suit.HEARTS));
    tempHand.add(new Card(Rank.FIVE, Suit.HEARTS));
    tempHand.add(new Card(Rank.NINE, Suit.SPADES));
    assertTrue(handTest.isTwoPair(tempHand));
  }

  @Test
  void testForRoyalFlush() {
    HandVariant handTest = new HandVariant();
    ArrayList<Card> tempHand = new ArrayList<>();


    tempHand.add(new Card(Rank.TEN, Suit.SPADES));
    tempHand.add(new Card(Rank.JACK, Suit.SPADES));
    tempHand.add(new Card(Rank.QUEEN, Suit.SPADES));
    tempHand.add(new Card(Rank.KING, Suit.SPADES));
    tempHand.add(new Card(Rank.ACE, Suit.SPADES));

    assertTrue(handTest.isRoyalFlush(tempHand));
  }

  @Test
  void testForFullHouse() {
    HandVariant handTest = new HandVariant();
    ArrayList<Card> tempHand = new ArrayList<>();

    tempHand.add(new Card(Rank.TEN, Suit.SPADES));
    tempHand.add(new Card(Rank.TEN, Suit.CLUBS));
    tempHand.add(new Card(Rank.QUEEN, Suit.HEARTS));
    tempHand.add(new Card(Rank.QUEEN, Suit.DIAMONDS));
    tempHand.add(new Card(Rank.QUEEN, Suit.CLUBS));

    assertTrue(handTest.isFullHouse(tempHand));
  }
}