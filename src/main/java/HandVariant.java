import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class HandVariant {

  //   ROYAL_FLUSH, STRAIGHT_FLUSH, FOUR_OF_A_KIND, FULL_HOUSE, FLUSH, STRAIGHT,
  //   THREE_OF_A_KIND, TWO_PAIR, JACKS_OR_BETTER

  ArrayList<Card> tempHand = new ArrayList<Card>(5);
  ArrayList<Card> hand = new ArrayList<Card>();

  public HandVariant() {

  }

  public boolean pairCard(ArrayList<Card> tempHand) {
    for (int i = 0; i < tempHand.size(); i++) {
      for (int j = 1; j < tempHand.size(); j++) {
        if ((i != j) && tempHand.get(i).getRank() == tempHand.get(j).getRank()) {
          return true;
        }
      }
    }
    return false;
  }

  public boolean isFlush(ArrayList<Card> tempHand) {
    for (int i = 0; i < tempHand.size(); i++) {
      if (tempHand.get(0).getSuit() == tempHand.get(1).getSuit() &&
              tempHand.get(1).getSuit() == tempHand.get(2).getSuit() &&
              tempHand.get(2).getSuit() == tempHand.get(3).getSuit() &&
              tempHand.get(3).getSuit() == tempHand.get(4).getSuit() &&
              tempHand.get(3).getSuit() == tempHand.get(4).getSuit()) {
        return true;
      }
    }
    return false;

  }

  // FourOfAKind
  public boolean isFourOfAKind(ArrayList<Card> tempHand){
    ArrayList<Card> handClone = (ArrayList<Card>) tempHand.clone();
    handClone = sortCards(handClone);
    int count = 0;

    if(handClone.get(0).getRank() == handClone.get(1).getRank()){
      for (int i = 1; i < handClone.size(); i++){
        if(handClone.get(0).getRank() == handClone.get(i).getRank()/* ||
                    handClone.get(1).getRank() == handClone.get(i++).getRank()*/){
          count++;
        }
      }
    }
    else if (!(handClone.get(0).getRank() == handClone.get(1).getRank())){
      if (handClone.get(1).getRank() == handClone.get(2).getRank()) {
        for (int i = 2; i < handClone.size(); i++) {
          if (handClone.get(1).getRank() == handClone.get(i).getRank()) {
            count++;
          }
        }
      }
    }
    if(count == 3){
      return true;
    }
    return false;
  }

  // FullHouse
  public boolean isFullHouse(ArrayList<Card> tempHand){
    ArrayList<Card> handClone = (ArrayList<Card>) tempHand.clone();
    handClone = sortCards(handClone);
    int count = 0;

    if(handClone.get(0).getRank() == handClone.get(1).getRank() &&
            !(handClone.get(1).getRank() == handClone.get(2).getRank())){
      for (int i = 3; i < handClone.size(); i++){
        if (handClone.get(2).getRank() == handClone.get(i).getRank()) {
          count++;
        }
      }
      if (count == 2) {
        if (handClone.get(3).getRank() == handClone.get(4).getRank()){
          return true;
        }
      }
    }
    else {
      for (int i = 0; i < 2; i++){
        if(handClone.get(0).getRank() == handClone.get(i).getRank()){
          count++;
        }
      }
      if (count == 2 && handClone.get(2).getRank() != handClone.get(3).getRank() &&
              handClone.get(3).getRank() == handClone.get(4).getRank()){
        return true;
      }
    }
    return false;
  }

  // ThreeOfAKind
  // - obs! måste användas efter man har kollat med isFourOfAKind och isFullHouse
  public boolean isThreeOfAKind(ArrayList<Card> tempHand) {
    ArrayList<Card> handClone = (ArrayList<Card>) tempHand.clone();
    handClone = sortCards(handClone);
    int count = 0;

    if (handClone.get(0).getRank() == handClone.get(1).getRank()) {
      for (int i = 1; i < handClone.size(); i++) {
        if (handClone.get(0).getRank() == handClone.get(i).getRank()) {
          count++;
        }
      }
    }
    else if (!(handClone.get(0).getRank() == handClone.get(1).getRank()) &&
            handClone.get(1).getRank() == handClone.get(2).getRank()) {
      for (int i = 2; i < handClone.size(); i++) {
        if (handClone.get(1).getRank() == handClone.get(i).getRank()) {
          count++;
        }
      }
    }
    else {
      for (int i = 3; i < handClone.size(); i++) {
        if (handClone.get(2).getRank() == handClone.get(i).getRank()) {
          count++;
        }
      }
    }
    if (count == 2) {
      System.out.println(count);
      return true;
    }
    return false;
  }

  // TwoPair
  public boolean isTwoPair(ArrayList<Card> tempHand) {
    ArrayList<Card> handClone = (ArrayList<Card>) tempHand.clone();
    handClone = sortCards(handClone);

    if(handClone.get(0).getRank() == handClone.get(1).getRank()){
      for (int i = 3; i < handClone.size(); i++) {
        if (handClone.get(2).getRank() == handClone.get(3).getRank() ||
                handClone.get(3).getRank() == handClone.get(4).getRank()) {
          return true;
        }
      }
    }
    else if(!(handClone.get(0).getRank() == handClone.get(1).getRank()) &&
            handClone.get(1).getRank() == handClone.get(2).getRank() &&
            handClone.get(3).getRank() == handClone.get(4).getRank()){
      return true;
    }
    return false;
  }

  // JacksOrBetter
  public boolean isJackOrBetter(ArrayList<Card> tempHand) {
    ArrayList<Card> handClone = (ArrayList<Card>) tempHand.clone();
    // handClone = sortCards(handClone);

    for(int i = 0; i < handClone.size(); i++){
      for (int j = 0; j < handClone.size(); j++) {
        if((i != j) && handClone.get(i).getRank() == handClone.get(j).getRank()) {
          if((handClone.get(i).getRank() == Rank.JACK) ||
                  (handClone.get(i).getRank() == Rank.QUEEN) ||
                  (handClone.get(i).getRank() == Rank.KING) ||
                  (handClone.get(i).getRank() == Rank.ACE)){
            return true;
          }
        }
      }
    }
    return false;
  }


  //	Royal Flush
  public boolean isRoyalFlush(ArrayList<Card> tempHand) {
    tempHand = sortCards(tempHand);
    for (int i = 0; i < tempHand.size(); i++) {

      if (tempHand.get(0).getSuit() == tempHand.get(1).getSuit() &&
              tempHand.get(1).getSuit() == tempHand.get(2).getSuit() &&
              tempHand.get(2).getSuit() == tempHand.get(3).getSuit() &&
              tempHand.get(3).getSuit() == tempHand.get(4).getSuit() &&
              tempHand.get(3).getSuit() == tempHand.get(4).getSuit()) {
        if (tempHand.get(0).getRank() == Rank.TEN &&
                tempHand.get(1).getRank() == Rank.JACK &&
                tempHand.get(2).getRank() == Rank.QUEEN &&
                tempHand.get(3).getRank() == Rank.KING &&
                tempHand.get(4).getRank() == Rank.ACE)
          return true;
      }
    }
    return false;
  }

  public boolean isStraightFlush(ArrayList<Card> tempHand) {
    tempHand = sortCards(tempHand);
    for (int i = 0; i < tempHand.size(); i++) {

      if (tempHand.get(0).getSuit() == tempHand.get(1).getSuit() &&
              tempHand.get(1).getSuit() == tempHand.get(2).getSuit() &&
              tempHand.get(2).getSuit() == tempHand.get(3).getSuit() &&
              tempHand.get(3).getSuit() == tempHand.get(4).getSuit() &&
              tempHand.get(3).getSuit() == tempHand.get(4).getSuit()) {
        if (tempHand.get(0).getRank().ordinal() != Rank.TWO.ordinal() &&
                tempHand.get(0).getRank().ordinal() <= Rank.NINE.ordinal() &&
                tempHand.get(1).getRank().ordinal() == tempHand.get(0).getRank().ordinal()+1 &&
                tempHand.get(2).getRank().ordinal() == tempHand.get(1).getRank().ordinal()+1 &&
                tempHand.get(3).getRank().ordinal() == tempHand.get(2).getRank().ordinal()+1 &&
                tempHand.get(4).getRank().ordinal() == tempHand.get(3).getRank().ordinal()+1

        )

          return true;
      }
      else if

      (tempHand.get(0).getRank() == Rank.TWO &&
                      tempHand.get(1).getRank() == Rank.THREE &&
                      tempHand.get(2).getRank() == Rank.FOUR &&
                      tempHand.get(3).getRank() == Rank.FIVE &&
                      tempHand.get(4).getRank() == Rank.ACE)
      {
        return true;
      }

    }



    return false;
  }

  public boolean isStraight(ArrayList<Card> tempHand){
    ArrayList<Card> handClone = (ArrayList<Card>) tempHand.clone();
    handClone = sortCards(handClone);
    int count = 0;
    System.out.println(handClone.get(1).getRank().ordinal());
    if(handClone.get(0).getRank().ordinal() <= Rank.NINE.ordinal()){
      for(int i = 1; i < handClone.size(); i++){
        if(handClone.get(i -1).getRank().ordinal() == handClone.get(i).getRank().ordinal()-1){

          count++;
        }
      }
      if(count == 4){
        return true;
      }
    }
    else if(handClone.get(4).getRank() == Rank.ACE){
      if(handClone.get(0).getRank() == Rank.TWO
              || handClone.get(0).getRank() == Rank.TEN){
        for(int i = 1; i < handClone.size() - 1; i++){
          if(handClone.get(i -1).getRank().ordinal() == handClone.get(i).getRank().ordinal()-1){
            count++;
          }
        }
        if(count == 3){
          return true;
        }
      }
    }
    return false;
  }


   /* public ArrayList<Card> sortCards(ArrayList<Card> cards){
    Collections.sort(cards, new Comparator<Card>() {
        @Override
        public int compare(Card o1, Card o2) {
            return o1.getRank().toString().compareTo(o2.getRank().toString());
        }
    });
        return cards;
    }*/

  public ArrayList<Card> sortCards(ArrayList<Card> cards){
    Collections.sort(cards, new Comparator<Card>() {
      @Override
      public int compare(Card o1, Card o2) {
        return o1.getRank().compareTo(o2.getRank());
      }
    });
    return cards;
  }

  public String result(ArrayList<Card> tempHand){

    if(pairCard(tempHand)){
      if(isFourOfAKind(tempHand)){
        return "Four of a Kind";
      }
      else if(isFullHouse(tempHand)){
        System.out.println(isFullHouse(tempHand));
        return "Full House";
      }
      else if(isThreeOfAKind(tempHand)){
        return "Three of a Kind";
      }
      else if(isTwoPair(tempHand)){
        return "Two Pair";
      }
      else if(isJackOrBetter(tempHand)) {
        return "Jacks or Better";
      }
    }
    else {
      if(isRoyalFlush(tempHand)){
        return "Royal Flush";
      }
      else if(isStraightFlush(tempHand)){
        return "Straight Flush";
      }
      else if(isFlush(tempHand)){
        return "Flush";
      }
      else if(isStraight(tempHand)){
        return "Straight";
      }
    }
    return "ingen kombination";

  }

  // Test metod for metoder
  public static void main(String[] args){

    Deck d = new Deck();
    Card c1 = new Card(Rank.JACK, Suit.DIAMONDS);
    Card c2 = new Card(Rank.TEN, Suit.CLUBS);
    Card c3 = new Card(Rank.QUEEN, Suit.SPADES);
    Card c4 = new Card(Rank.ACE, Suit.DIAMONDS);
    Card c5 = new Card(Rank.THREE, Suit.HEARTS);

    ArrayList<Card> tempHand = new ArrayList<>();
    tempHand.add(c1);
    tempHand.add(c2);
    tempHand.add(c3);
    tempHand.add(c4);
    tempHand.add(c5);

    HandVariant h = new HandVariant();

    ArrayList<Card> handClone = (ArrayList<Card>) tempHand.clone();
    //handClone = h.sortCards(handClone);
    h.sortCards(tempHand);
    for(Card card : tempHand){
      System.out.println(card.toString());
    }

    System.out.println(h.isStraight(tempHand));
    System.out.println(h.result(tempHand));


  }


}