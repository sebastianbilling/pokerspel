import java.util.ArrayList;
import java.util.Scanner;

public class Videopoker {
  int vinster, förluster, oavgjort, insats, bet = 0;
  private static Videopoker instance;

  ArrayList<Card> hand = new ArrayList<>();
  ArrayList<Card> tempHand = new ArrayList<Card>();

  Deck deck = new Deck();

  public Videopoker() {

    boolean playing = true;
    HandVariant handVariant = new HandVariant();

    while (playing = true) {

      deck.shuffle();

      hand.add(deck.draw());
      hand.add(deck.draw());
      hand.add(deck.draw());
      hand.add(deck.draw());
      hand.add(deck.draw());

      Scanner hold = new Scanner(System.in);
      System.out.println("                        88                              \r\n" +
              "                        88                              \r\n" +
              "                        88                              \r\n" +
              "8b,dPPYba,   ,adPPYba,  88   ,d8  ,adPPYba, 8b,dPPYba,  \r\n" +
              "88P'    \"8a a8\"     \"8a 88 ,a8\"  a8P_____88 88P'   \"Y8  \r\n" +
              "88       d8 8b       d8 8888[    8PP\"\"\"\"\"\"\" 88          \r\n" +
              "88b,   ,a8\" \"8a,   ,a8\" 88`\"Yba, \"8b,   ,aa 88          \r\n" +
              "88`YbbdP\"'   `\"YbbdP\"'  88   `Y8a `\"Ybbd8\"' 88          \r\n" +
              "88                                                      \r\n" +
              "88                                                      \n");
      System.out.println("                    __                                               \r\n" +
              "              _..-''--'----_.                                        \r\n" +
              "            ,''.-''| .---/ _`-._                                     \r\n" +
              "          ,' \\ \\  ;| | ,/ / `-._`-.                                  \r\n" +
              "        ,' ,',\\ \\( | |// /,-._  / /                                  \r\n" +
              "        ;.`. `,\\ \\`| |/ / |   )/ /                                   \r\n" +
              "       / /`_`.\\_\\ \\| /_.-.'-''/ /                                    \r\n" +
              "      / /_|_:.`. \\ |;'`..')  / /                                     \r\n" +
              "      `-._`-._`.`.;`.\\  ,'  / /                                      \r\n" +
              "          `-._`.`/    ,'-._/ /                                       \r\n" +
              "            : `-/     \\`-.._/                                        \r\n" +
              "            |  :      ;._ (                                          \r\n" +
              "            :  |      \\  ` \\                                         \r\n" +
              "             \\         \\   |                                         \r\n" +
              "              :        :   ;                                         \r\n" +
              "              |           /                                          \r\n" +
              "              ;         ,'                                           \r\n" +
              "             /         /                                             \r\n" +
              "            /         /                                              \r\n" +
              "                     /        \n\nVälkommen till Videopoker-spelet!\n");




      System.out.println("Du har följande kort: 1. " + hand.get(0) + ", 2. " + hand.get(1) + ", 3. " + hand.get(2) + ", 4. " + hand.get(3) + ", 5. " + hand.get(4));
      System.out.println("\nAnge nummer på de kort du vill behålla:");

      String nextLine = hold.nextLine();

      if(nextLine.contains("1")) {
        tempHand.add(hand.get(0));
      }
      if(nextLine.contains("2")) {
        tempHand.add(hand.get(1));
      }
      if(nextLine.contains("3")) {
        tempHand.add(hand.get(2));
      }
      if(nextLine.contains("4")) {
        tempHand.add(hand.get(3));
      }
      if(nextLine.contains("5")) {
        tempHand.add(hand.get(4));
      }

      int nyaKort = 5 - tempHand.size();

      for (int i = 0; i < nyaKort; i++) {
        tempHand.add(deck.draw());
      }

      System.out.println("\nDu har följande kort: 1. " + tempHand.get(0) + ", 2. " + tempHand.get(1) + ", 3. " + tempHand.get(2) + ", 4. " + tempHand.get(3) + ", 5. " + tempHand.get(4));

      System.out.println("Du har " + handVariant.result(tempHand) + ".");


      System.out.println("\nTryck 1 för att spela en vända till, tryck 2 för att avsluta.\n");


      Scanner reset = new Scanner(System.in);
      String resetOrContinue = reset.nextLine();

      if(resetOrContinue.contains("2")) {
        System.out.println("\nTack för att du spelade, välkommen åter!\n");
        System.out.println("                                      .------.\r\n" +
                "                   .------.           |A .   |\r\n" +
                "                   |A_  _ |    .------; / \\  |\r\n" +
                "                   |( \\/ )|-----. _   |(_,_) |\r\n" +
                "                   | \\  / | /\\  |( )  |  I  A|\r\n" +
                "                   |  \\/ A|/  \\ |_x_) |------'\r\n" +
                "                   `-----+'\\  / | Y  A|\r\n" +
                "                         |  \\/ A|-----'       \r\n" +
                "                         `------'\r\n" +
                "");
        System.exit(0);
      }
      else { Videopoker videopoker = new Videopoker();
      }}
  }


  public static Videopoker getInstance() {
    if (instance == null) {
      instance = new Videopoker();
    }
    return instance;
  }


/*
	handValue bestämmer vilket värde hhanden har samt vilken multiplier spelarens vinst kommer ha.
	public void checkValueInHand(){
		handValue = 9;
		if (ROYAL_FLUSH())
			handValue = 9;
		else if (STRAIGHT_FLUSH())
			handValue = 8;
		else if (FOUR_OF_A_KUND())
			handValue = 7;
		else if (FULL_HOUSE())
			handValue = 6;
		else if (FLUSH())
			handValue= 5;
		else if (STRAIGHT())
			handValue = 4;
		else if (TRHEE_OF_A_KUND())
			handValue = 3;
		else if (TWO_PAIR)
			handValue = 2;
		else if (JACKS_OR_BETTER)
			handValue = 1;
		else
			handValue = 0;
		if (handValue < 9)
			System.out.println(handValue + "!");
		else System.out.println("You lost!");
	}
	*/





  public int insats() {
    Scanner insatsin = new Scanner(System.in);
    System.out.println("              __                                               \r\n" +
            "        _..-''--'----_.                                        \r\n" +
            "      ,''.-''| .---/ _`-._                                     \r\n" +
            "    ,' \\ \\  ;| | ,/ / `-._`-.                                  \r\n" +
            "  ,' ,',\\ \\( | |// /,-._  / /                                  \r\n" +
            "  ;.`. `,\\ \\`| |/ / |   )/ /                                   \r\n" +
            " / /`_`.\\_\\ \\| /_.-.'-''/ /                                    \r\n" +
            "/ /_|_:.`. \\ |;'`..')  / /                                     \r\n" +
            "`-._`-._`.`.;`.\\  ,'  / /                                      \r\n" +
            "    `-._`.`/    ,'-._/ /                                       \r\n" +
            "      : `-/     \\`-.._/                                        \r\n" +
            "      |  :      ;._ (                                          \r\n" +
            "      :  |      \\  ` \\                                         \r\n" +
            "       \\         \\   |                                         \r\n" +
            "        :        :   ;                                         \r\n" +
            "        |           /                                          \r\n" +
            "        ;         ,'                                           \r\n" +
            "       /         /                                             \r\n" +
            "      /         /                                              \r\n" +
            "               /        \nVälkommen till Videopoker-spelet!\nAnge hur mycket du vill lägga in som insats:");
    insats = insatsin.nextInt();
    return insats;

  }
  public int bet() {
    Scanner betin = new Scanner(System.in);
    System.out.println("Ange hur mycket av din insats du vill betta med:");
    this.bet = betin.nextInt();
    return bet;
  }
}